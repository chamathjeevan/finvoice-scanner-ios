# Finvoice

Framework used to scan invoice values from an image.



## Before you start coding

This is required to work with the project:

- [Homebrew](https://brew.sh)
- [Xcode](https://developer.apple.com/xcode)
- Bazel (version 0.26)

### 1. Brew
```
brew bundle
```

### 2. Install gems
```
gem install bundler
```

### 3. Install packages

```
pip install -U --user six numpy wheel setuptools mock future>=0.17.1
pip install -U --user keras_applications==1.0.6 --no-deps
pip install -U --user keras_preprocessing==1.0.5 --no-deps
```
### 4. Install dependencies

We depend on some submodules and pods. Run following script to install these dependencies.
```
make install-dependencies
```

### 5. Add private pod

```
pod repo add FinvoiceCocoaPodsSpec git@github.com:nicknamed-ab/finvoice-cocoapods-spec.git
```



## Update dependencies

1. Set versions for dependencies:
- Update pod versions in [podfile](./Podfile)
- Update variable `C_LIBRARY_TAG` in [Common.sh](./Scripts/Common.sh) to update version of the C++ library.
2. Run script
```
make update-dependencies
```



## Release

Release project to GitHub and Cocoapods (private) by running following script:

```
fastlane release
```



## Use framework

### Import

Use CocoaPods to import this framework. Read more about CocoaPods [here](https://cocoapods.org).

1. Add the source to the podfile.

   `source 'https://github.com/nicknamed-ab/finvoice-cocoapods-spec.git'`

2. Add the pod to the podfile.

   `pod 'Finvoice'`
   
3. Enable address sanitizer for scheme if the build is a **debug** build

Your podfile should look something like this:
```
platform :ios, '9.0'
use_frameworks!

source 'https://github.com/nicknamed-ab/finvoice-cocoapods-spec.git'
source 'https://github.com/CocoaPods/Specs.git'

target 'MyApp' do
  pod 'Finvoice'
end
```
### Scan video
```Swift
let invoiceScanner = InvoiceScanner()
let captureDevice = AVCaptureDevice.default(for: .video)

func captureOutput(
        _ output: AVCaptureOutput,
        didOutput sampleBuffer: CMSampleBuffer,
        from connection: AVCaptureConnection) {

        // Set image orientation
  
        let cameraPosition = captureDevice.position
        let imageOrientation: CGImagePropertyOrientation

        switch UIDevice.current.orientation {
        case .portrait:
            imageOrientation = cameraPosition == .front ? .leftMirrored : .right
        case .landscapeLeft:
            imageOrientation = cameraPosition == .front ? .downMirrored : .up
        case .portraitUpsideDown:
            imageOrientation = cameraPosition == .front ? .rightMirrored : .left
        case .landscapeRight:
            imageOrientation = cameraPosition == .front ? .upMirrored : .down
        default:
            imageOrientation = cameraPosition == .front ? .leftMirrored : .right
        }
  
        // Create an invoice page
  
        let page = InvoicePage(
          sampleBuffer: sampleBuffer, 
          imageOrientation: imageOrientation
        )
  
        // Scan page for predictions

        invoiceScanner.scan(page) { result in

            switch result {
            case .success(let predictions):
                // Handle predictions
            case .failure:
                // Handle error
            }
        }
    }
```
