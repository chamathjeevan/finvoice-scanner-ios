//
//  FIResult.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-09.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FIPrediction.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(InvoiceScanningResult)
@interface FIResult : NSObject

@property (nonatomic, retain) FIPrediction* accountPrediction;
@property (nonatomic, retain) FIPrediction* amountPrediction;
@property (nonatomic, retain) FIPrediction* referencePrediction;
@property (nonatomic, retain) FIPrediction* datePrediction;

- (instancetype)initWithAmountPrediction: (FIPrediction *)amountPrediction
                       accountPrediction: (FIPrediction *)accountPrediction
                     referencePrediction: (FIPrediction *)referencePrediction
                          datePrediction: (FIPrediction *)datePrediction;

@end

NS_ASSUME_NONNULL_END
