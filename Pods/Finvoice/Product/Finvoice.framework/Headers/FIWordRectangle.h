//
//  FIWordRectangle.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-09.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <CoreGraphics/CoreGraphics.h>

struct FIWordRectangle {
    CGPoint topLeft;
    CGPoint topRight;
    CGPoint bottomRight;
    CGPoint bottomLeft;
} NS_SWIFT_NAME(InvoiceWordRectangle);
