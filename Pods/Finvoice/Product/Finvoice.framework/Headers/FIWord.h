//
//  FIWord.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-09.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(InvoiceScanningWord)
@interface FIWord : NSObject

@property (nonatomic, retain) NSString *text;
@property (nonatomic, assign) struct FIWordRectangle boundingBox;

- (instancetype)initWithText: (NSString *)text boundingBox:(struct FIWordRectangle)boundingBox;

@end

NS_ASSUME_NONNULL_END
