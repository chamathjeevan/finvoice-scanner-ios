//
//  FIInvoiceScanner.h
//  Finvoice
//
//  Created by David Boeryd on 2019-08-27.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FIResult.h"
#import "FIInvoicePage.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(InvoiceScanner)
@interface FIInvoiceScanner : NSObject

@property (readonly, strong, nonatomic) NSString *cppFrameworkVersion;
@property (nonatomic, assign) BOOL isScanning;

- (void)scanPage:(FIInvoicePage *)page
 completionBlock:(void (^)(FIResult * _Nullable, NSError * _Nullable))completionBlock
NS_REFINED_FOR_SWIFT;

- (void)scanPages:(NSSet<FIInvoicePage *> *)pages
 completionBlock:(void (^)(FIResult * _Nullable, NSError * _Nullable))completionBlock
NS_REFINED_FOR_SWIFT;


@end

NS_ASSUME_NONNULL_END
