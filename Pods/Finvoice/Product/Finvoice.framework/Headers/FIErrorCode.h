//
//  FIErrorCode.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-10-02.
//  Copyright © 2019 Nicknamed AB. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, FIErrorCode) {
    kFIErrorUnkown,
    kFIErrorInvoiceNotFound,
    kFIErrorScannerAlreayInUse,
    kFIErrorOrientationNotSupported
} NS_SWIFT_NAME(FinvoiceErrorCode);
