//
//  FIError.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-10-02.
//  Copyright © 2019 Nicknamed AB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FIErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface FIError : NSError

@property (readonly, nonatomic) FIErrorCode invoiceErrorCode;

- (instancetype)initWithCode:(FIErrorCode)code NS_DESIGNATED_INITIALIZER;

- (instancetype)initWithDomain:(NSErrorDomain)domain
    code:(NSInteger)code
userInfo:(nullable NSDictionary<NSErrorUserInfoKey,id> *)dict NS_UNAVAILABLE;

+ (FIError *)errorWithCode:(FIErrorCode)code;

+ (instancetype)errorWithDomain:(NSErrorDomain)domain
                           code:(NSInteger)code
                       userInfo:(nullable NSDictionary<NSErrorUserInfoKey,id> *)dict NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
