//
//  FIPrediction.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-09.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FIWord.h"

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(InvoiceScanningPrediction)
@interface FIPrediction : NSObject

@property (nonatomic, retain) FIWord *word;
@property (nonatomic, assign) float probability;

- (instancetype)initWithWord: (FIWord *)word probability:(float)probability;

@end

NS_ASSUME_NONNULL_END
