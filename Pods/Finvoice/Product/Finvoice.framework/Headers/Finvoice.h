//
//  Finvoice.h
//  Finvoice
//
//  Created by David Boeryd on 2019-08-27.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Finvoice.
FOUNDATION_EXPORT double FinvoiceVersionNumber;

//! Project version string for Finvoice.
FOUNDATION_EXPORT const unsigned char FinvoiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <invoiceScanningLibrary/PublicHeader.h>

#import <Finvoice/FIInvoiceScanner.h>
#import <Finvoice/FIWordRectangle.h>
#import <Finvoice/FIConstants.h>
#import <Finvoice/FIError.h>
