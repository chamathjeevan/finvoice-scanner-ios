//
//  FIInvoicePage.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-24.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

NS_SWIFT_NAME(InvoicePage)
@interface FIInvoicePage : NSObject

- (instancetype)init NS_UNAVAILABLE;

- (instancetype)initWithImage:(UIImage *)image
NS_REFINED_FOR_SWIFT;

- (instancetype)initWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
                    imageOrientation:(CGImagePropertyOrientation)imageOrientation
NS_REFINED_FOR_SWIFT;

@end

NS_ASSUME_NONNULL_END
