//
//  FIConstants.h
//  Finvoice
//
//  Created by Marcus Brissman on 2019-09-11.
//  Copyright © 2019 David Boeryd. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const kFIErrorDomain;
